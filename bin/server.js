//Load require packages
var express = require('express');
var mongoose = require('mongoose');
var config = require('../config');
var bodyParser = require('body-parser');
var passport = require('passport');

var app = express(); //Create the Express app
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(passport.initialize());

//routes are defined here
var apps = require('../routes/applications');


var urlMongoose = config.Mongo.client + "://" + config.Mongo.host + ":" + config.Mongo.port + "/" + config.Mongo.dbName;

mongoose.connect(urlMongoose, function (err) {
    if (err) throw err;
});

var port = process.env.PORT || 8000;

// Create our Express router
var router = express.Router();

// Initial dummy route for testing
router.get('/', function (req, res) {
    res.json({message: 'App Working!'});
});

// Register all our routes with /api
app.use('/api', router);
app.use('/api/applications', apps);


//error handler para todas las rutas
app.use(function (err, req, res, next) {
    if (err) {
        console.log(err);
        res.status(500).send(err);
    }
});

// Start the server
app.listen(port);

exports.app = app;

console.log('Express is running in port: ' + port);